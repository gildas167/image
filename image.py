import cv2
import time

def capture_and_save_image(folder_path):
    # Capturez une image depuis la caméra
    cap = cv2.VideoCapture(0)  # Utilisez l'index de la caméra approprié (0 pour la caméra par défaut)
    ret, frame = cap.read()

    # Vérifiez si la capture a réussi
    if ret:
        # Générez un nom de fichier unique basé sur la date et l'heure actuelles
        timestamp = time.strftime("%Y%m%d_%H%M%S")
        image_name = f"image_{timestamp}.jpg"

        # Construisez le chemin complet du fichier en utilisant le dossier spécifié
        image_path = folder_path + image_name

        # Enregistrez l'image dans le dossier
        cv2.imwrite(image_path, frame)
        print(f"Image enregistrée sous {image_path}")

        # Libérez les ressources de la caméra
        cap.release()
    else:
        print("La capture d'image a échoué.")

# Exemple d'utilisation : Remplacez '/chemin/vers/le/dossier/images/' par le chemin réel du dossier
capture_and_save_image('/home/pi/adeept_rasptank/images/')
